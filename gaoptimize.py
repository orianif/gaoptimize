# -*- coding: utf-8 -*-
"""
GAOPTIMIZE.py
A basic implementation of the Genetic Algorithm for optimization of mixed-integer
problems. Set up the algorithm in the "OPTIMIZATION PARAMETERS" section. The code
is largely explained in the comments.

ALGORITHM:

1) GENERATE AN INITIAL POPULATION: the parameter values (chromosomes) of all 
solutions (individuals) are randomly draw from a uniform distribution 
inside the range indicated by the user.

2) COMPUTE THE OBJECTIVE (FITNESS) FUNCTION for each solution as defined by the user.

3) RETRIEVE THE BEST SOLUTION: if the number of generations has reached the 
prescribed limit, return the best solution and stop. Also stop if
the objectve funtion of the best solution has not varied significatnly for a 
prescribed number of generations.

4) MULTI-PARENT MULTI-POINT CROSSOVER USING LINEAR OR FITNESS BASED RANKING
the parameter values of all solutions from the initial/previous population are 
randomly crossed with probability following a linear ranking of or directly propåortional to 
the objective (fitnness) function values (see e.g. Baker J. E., “Adaptive Selection Methods 
for Genetic Algorithms”, Proc. ICGA 1, plt. 101-111, 1985.).

5) RANDOM MUTATION: randomly choose auser-defined fraction of the parameter values 
to mutate. The mutated parameters are randomly drawn from the uniform distribution
within the user-defined boundaries.

6) ELITIST SELECTION preserve in the population the best individuals generated until now

7) ITERATE FROM STEP 2) 

NOTE: by setting up the optimization parameter "parlog", parameter values can be 
transformed to log during steps 1) and 5). In some cases this operation allows a 
more efficient convergence to minima.

VERISON: 0.1 09/2017

Fabio Oriani, Geological Survey of Denmark and Greenland
fabio.oriani@protonmail.com
"""
#%% PACKAGE IMPORT
from joblib import Parallel, delayed
import time
import numpy as np
import matplotlib.pyplot as plt

#%% OTHER FUNCTIONS
def logrand(base=np.exp(1),size=None): #generate log-uniform number in [0-1]
    if np.shape(base)[0]!=size[1]:
        return("error in logrand: base length should be equal to size[1]")
    ran=np.random.uniform(0,1,size)
    out=np.zeros(np.shape(ran))
    for i in range(0,len(base)):
        l=np.power(base[i],[0,1])
        out[:,i]=(np.power(base[i],ran[:,i])-min(l))/(max(l)-min(l))
    return out

#%% OPTIMIZATION FUNCTION
def optimize(pars,objf):
    """
    Optimization function using the Genetic Algorythm (GA).
    
    INPUT:
    objf: user-defined objective function to minimize using the parameters.
        the number and order of parameters correspond to the ones defined in 
        pars[inset]
        
    pars, dictionary containing the GA parameters. It must contain:
            
        pars["npar"] # [positive int scalar] number of parameters (genes) 
        to optimize
        
        pars["inset"] # [array of length npar] initial parameter set

        
        pars["lb"] # [float array of length npar] lower boundary for each 
        parameter
        
        pars["ub"] # [float array of length npar] upper (exclusive) boundary 
        for each parameter
        
        pars["parint"] # [0/1 int array of length npar] variable indicating 
        wether a parameter is integer(1)/float(0) valued
        
        pars["parlog"] # [0/1 int array of length npar] variable indicating 
        wether to apply (1) or not float(0) a log transformation of the 
        parameters.
        
        pars["logbase"] # [positive scalar] base given for the log 
        transformation in the search space (costant for all transformed 
        parameters)
                
        pars["popsize"] # [positive int scalar] number of solutions 
        (individuals) drawn to form the initial and following populations
        
        pars["ngen"] # [positive int scalar] number of generations before 
        ending the optimization
        
        pars["obj_th"] # [positive scalar] the algorithm also stops if 
        for gen_th generations: old_best-new_best<obj_th
        
        pars["gen_th"] # [positive scalar]
        
        pars["mut_rate"] # [scalar in [0,1]] fraction (probability) 
        of the parameter values to be randomly drawn at each generation
        
        pars["lin_rank"] # [0/1 scalar] linear (1) / fitness-based (0) 
        ranking switch
        
        pars["num_jobs"] # [positive scalar] cores used parallel job 
        computing, 1 for linear computation 
        
        pars["display"] # [positive scalar] display the evolution over 
        the function (2D only)
        
        pars["disp_pts"] # [positive scalar] number of points where to 
        evaluate the objective function in each dimension (2 dims) for 
        background display map 
        
        pars["gnoise"] # [0/1 int array of length npar] variable indicating
        wether to apply (1) or not float(0) a gaussian noise to the new 
        generation
        
        pars["noisevar"]  # [float array of length npar] vector containing 
        the variance of the noise to apply to each (only applied if gnoise==1)    
    
    """
    startt=time.time()
    # input variables
    npar=pars["npar"] # [positive int scalar] number of paramters (genes) to optimize
    lb=pars["lb"] # [float array of length npar] lower boundary for each parameter
    ub=pars["ub"] # [float array of length npar] upper (exclusive) boundary for each parameter
    parint=pars["parint"] # [0/1 int array of length npar] variable indicating wether a parameter is integer(1)/float(0) valued
    parlog=pars["parlog"] # [0/1 int array of length npar] variable indicating wether to apply (1) or not float(0) a log transformation of the parameters.
    logbase=pars["logbase"] # [positive scalar] base given for the log transformation in the search space (costant for all transformed parameters)
    inset=pars["inset"] # [array of length npar] initial parameter set
    popsize=pars["popsize"] # [positive int scalar] number of solutions (individuals) drawn to form the initial and following populations
    ngen=pars["ngen"] # [positive int scalar] number of generations before ending the optimization
    obj_th=pars["obj_th"] # [positive scalar] the algorithm also stops if for gen_th generations: old_best-new_best<obj_th
    gen_th=pars["gen_th"] # [positive scalar]
    mut_rate=pars["mut_rate"] # [scalar in [0,1]] fraction (probability) of the parameter values to be randomly drawn at each generation
    lin_rank=pars["lin_rank"] # [0/1 scalar] linear (1) / fitness-based (0) ranking switch
    num_jobs=pars["num_jobs"] # [positive scalar] cores used parallel job computing, 1 for linear computation 
    display=pars["display"] # [positive scalar] display the evolution over the function (2D only)
    disp_pts=pars["disp_pts"] # [positive scalar] number of points where to evaluate the objective function in each dimension (2 dims) for background display map 
    gnoise=pars["gnoise"] # [0/1 int array of length npar] variable indicating wether to apply (1) or not float(0) a gaussian noise to the new generation
    noisevar=pars["noisevar"]  # [float array of length npar] vector containing the variance of the noise to apply to each (only applied if gnoise==1)
    
    # INPUT CHECKS
    if not len(lb)==len(ub)==len(parint)==len(parlog)==len(inset)==len(logbase)==npar:
        return print("ERROR: invalid input parameter length!")
    
    if display==1 and npar!=2:
        return print("ERROR: display mode can only operate if npar==2!")
    
    # START OPTIMIZATION    
    print("############# OPTIMIZATION STARTED AT TIME ", time.strftime("%y/%m/%d %H:%M:%S"),"##############")    
    #%% GENERATE A RANDOM INITIAL POPULATION (including initial set)
    lbp=np.array([lb]*popsize) # lower boundary for population
    ubp=np.array([ub]*popsize) # lower boundary for population
    #random int/float among the given boundaries
    pop=lbp+np.random.rand(popsize, npar)*(ub-lb) # generate random population
    logpop=lbp+logrand(logbase,[popsize,npar])*(ub-lb) # generate random population using loguniform distribution
    pop[:,parlog>0]=logpop[:,parlog>0] #  # insert log-distributed parameters      
    pop[:,parint>0]=np.around(pop[:,parint>0]) # round values for int-type parameters
    pop[0,:]=inset # add intial set
    par_index=np.array([np.arange(0,npar)]*popsize) # lin index of each parameter (used for crossover)
    obj_out=np.zeros(popsize) # objective function preallocation
    n=0 # initialize counter for gen_th stopping criterion
    best_obj_out=np.array([]) # initialize best objective function
    
    #%% MAIN EVOLUTION LOOP
    for j in range(0,ngen):
        #%% EVALUATE FITNESS
        if j==0:
            inputs=range(0,popsize)
            obj_out = Parallel(n_jobs=num_jobs,batch_size=1)(delayed(objf)(pop[i,:],i) for i in inputs)
            obj_out=np.array(obj_out)
            old_best_obj_out=np.array(obj_out[0]) # take the initial solution as best
        else:
            inputs=range(1,popsize)
            obj_out_tmp=Parallel(n_jobs=num_jobs,batch_size=1)(delayed(objf)(pop[i,:],i) for i in inputs)
            obj_out=np.append(obj_out[0],obj_out_tmp)
            old_best_obj_out=best_obj_out # set up the last best solution as the old one
            
        #%% FIND BEST SOLUTION
        obj_sort_ind=np.array(np.argsort(obj_out)) # indexes of the sorted elements
        obj_sort_ind=obj_sort_ind[::-1]
        best=np.array(pop[obj_sort_ind[popsize-1],:]) # best solution
        best_obj_out=np.array(obj_out[obj_sort_ind[popsize-1]]) # best objective function value
        print("generation",j,"best objective function value:",best_obj_out)    
        if old_best_obj_out-best_obj_out<obj_th: #stopping rule
            n=n+1
        else:
            n=0 
        if n==gen_th:
            break
        
        #%% CROSSOVER USING LINEAR OR FITNESS-BASED RANpop=pop[obj_sort_ind[sel_ind],par_index] #KING
        if lin_rank==1: # LINEAR RANKING
            sp=np.linspace(1/popsize,1,popsize) # probability of being selected following a linear pdf
        else: # FITNESS-BASED RANKING
            sp=obj_out[obj_sort_ind]-best_obj_out# probability of being selected following a pdf proportional to fitness
            sp=1-sp/sp[0]
        cp=np.cumsum(sp/sum(sp)) # cumulative probability
        sel_ind=np.searchsorted(cp,np.random.rand(popsize,npar)) # random slection of each chromosomes among all individuals
        newpop=np.array(pop[obj_sort_ind[sel_ind],par_index]) # crossed population
        
        #%% MUTATION
        ranpop=lbp+np.random.rand(popsize, npar)*(ub-lb) # generate a random population
        logpop=lbp+logrand(logbase,[popsize,npar])*(ub-lb) # generate random population using loguniform distribution        
        ranpop[:,parlog>0]=logpop[:,parlog>0] # insert log-distributed parameters        
        mut_ind=np.random.rand(popsize,npar)<=mut_rate # random chromosome index to mutate
        newpop[mut_ind]=np.array(ranpop[mut_ind]) # mutate the prevouly crossed population
        
        #%% ADD GAUSSIAN NOISE
        if any(gnoise):
            mean = np.zeros(npar) # mean
            cov = np.identity(npar)*noisevar  # covariance
            eps = np.random.multivariate_normal(mean, cov, popsize) # noise
            newpop[:,gnoise>0]=newpop[:,gnoise>0]+eps[:,gnoise>0]
            newpop[newpop<lb]=lbp[newpop<lb] # assign boundary values where noise gets outside
            newpop[newpop>ub]=ubp[newpop>ub]
        
        #%% ELITIST SELECTION
        newpop[0,:]=best # substitute one solution with the best found until now
        obj_out[0]=best_obj_out        
        
        #%% ROUND FOR INTEGER PARAMS AND ASSIGN THE NEW POP AS CURRENT POP
        newpop[:,parint>0]=np.around(newpop[:,parint>0]) # round values for int-type parameters
        pop=newpop
        #%% PLOT (OPTIMAL)
        if display==1:
            if j==0: # evaluate the objective funtion on a regular grid to give a contour map
                x_ = np.linspace(lb[0],ub[0],disp_pts)
                y_ = np.linspace(ub[1],lb[1],disp_pts)
                x, y= np.meshgrid(x_, y_, indexing='xy')                
                mappop=np.vstack([np.ravel(x),np.ravel(y)]).T # generate grid of points
                mappop[:,parint[:2]>0]=np.around(mappop[:,parint[:2]>0]) # round values for int-type parameters
                mapobj_out=np.zeros(disp_pts*disp_pts) # objective function preallocation
                inputs=range(0,disp_pts*disp_pts)
                mapobj_out = Parallel(n_jobs=num_jobs)(delayed(objf)(mappop[i,:],i) for i in inputs)
                mapobj_out=np.array(mapobj_out)
                mapobj_out=mapobj_out.reshape((disp_pts,disp_pts))
                X=np.reshape(mappop[:,0],mapobj_out.shape)
                Y=np.reshape(mappop[:,1],mapobj_out.shape)
                kwargs = dict(extent=(lb[0], ub[0], lb[1], ub[1]), cmap='hot', origin='lower')
                plt.clf()
            plt.cla()           
            #plt.contourf(X,Y,mapobj_out,np.linspace(min(np.ravel(mapobj_out)),max(np.ravel(mapobj_out)),25),**kwargs)
            plt.contourf(X,Y,mapobj_out,**kwargs)
            if j==0:
                plt.colorbar() 
            plt.plot(pop[:,0],pop[:,1],"o")
            plt.xlabel('parameter 1')
            plt.ylabel('parameter 2')
            plt.plot(best[0],best[1],"go")
            plt.axis([lb[0], ub[0], lb[1], ub[1]])
            plt.ion()
            plt.show()
            plt.pause(0.005) 
            
    #%%STOP THE ALGORITHM
    endt=time.time()
    print ("############# OPTIMIZATION ENDED AT TIME ", time.strftime("%y/%m/%d %H:%M:%S"), "##############")
    print("best individual found is", best)
    print("with object function = ", best_obj_out)
    print("reason for stopping:")
    if n==gen_th:
        print("objective function was stable for ", gen_th, " generations")
    else:
        print("maximum number of generations reached (", ngen, ")")
    print("duration",endt-startt)
    return best
