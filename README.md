# GAoptimize

### What is this repository for? ###

A basic implementation of the Genetic Algorithm [1] for optimization of mixed-integer
problems. Set up the algorithm in the "OPTIMIZATION PARAMETERS" section. For usage instruction
see the help in the optimize.py and the usage examples in optim_test.py.

ALGORITHM:

1) GENERATE AN INITIAL POPULATION: the parameter values (chromosomes) of all 
solutions (individuals) are randomly draw from a uniform distribution 
inside the range indicated by the user.

2) COMPUTE THE OBJECTIVE (FITNESS) FUNCTION for each solution as defined by the user.

3) RETRIEVE THE BEST SOLUTION: if the number of generations has reached the 
prescribed limit, return the best solution and stop. Also stop if
the objectve funtion of the best solution has not varied significatnly for a 
prescribed number of generations.

4) MULTI-PARENT MULTI-POINT CROSSOVER USING LINEAR OR FITNESS BASED RANKING
the parameter values of all solutions from the initial/previous population are 
randomly crossed with probability following a linear ranking of or directly propåortional to 
the objective (fitnness) function values (see e.g. Baker J. E., “Adaptive Selection Methods 
for Genetic Algorithms”, Proc. ICGA 1, plt. 101-111, 1985.).

5) RANDOM MUTATION: randomly choose auser-defined fraction of the parameter values 
to mutate. The mutated parameters are randomly drawn from the uniform distribution
within the user-defined boundaries.

6) ELITIST SELECTION preserve in the population the best individuals generated until now

7) ITERATE FROM STEP 2) 

NOTES: 

_ by setting up the optimization parameter "parlog", parameter values can be 
transformed to log during steps 1) and 5). In some cases this operation allows a 
more efficient convergence to minima.

_ use the display mode only for illustrative purposes (2D case only).

VERISON: 0.1 09/2017

Fabio Oriani, Geological Survey of Denmark and Greenland
fabio.oriani@protonmail.com

[1] Chipperfield A, Fleming P, Fonseca C (1994) Genetic algorithm tools for control systems engineering. In: Proceedings of adaptive computing in engineering design and control. Citeseer, pp 128–133

### How to begin ###

To begin using the software:

_ check that the dependencies are satisfied on your machine (see below);

_ download the repository content. If you prefer, you can put the folder gaoptimize in your python path or leave it in the local folder;

_ open and run the file optim_test.py

_ setup the optimization parameters by following the instruction in the optimize function help

### Dependencies ###

_ Python 3

_ Python packages: joblib, time, numpy, matplotlib

### Need help? ###

Write Fabio : fabio (dot) oriani (at) protonmail (dot com)

### How to cite this software ###

Fabio Oriani, Gaoptimize software, Geological Survey fo Denmark and Greenland, 
https://doi.org/10.5281/zenodo.3427962

### License ###

Copyright (C) 2017  Fabio Oriani

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.