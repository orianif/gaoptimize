# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 18:07:52 2017

@author: fabio oriani, fabio.oriani@protonmail.com
"""
import numpy as np
import pylab as pl
import gaoptimize as ga

# TEST THE GENETIC ALGORITHM OVER DIFFERENT TEST FUNCTIONS

#%% ############### OPTIMIZATION PARAMETERS ###############################
pars=dict()
pars["npar"]=2 # [positive int scalar] number of parameters (genes) to optimize
pars["lb"]=np.array([-10,-10]) # [float array of length npar] lower boundary for each parameter
pars["ub"]=np.array([10,10]) # [float array of length npar] upper (exclusive) boundary for each parameter
pars["parint"]=np.array([0,0]) # [0/1 int array of length npar] variable indicating wether a parameter is integer(1)/float(0) valued
pars["parlog"]=np.array([0,0]) # [0/1 int array of length npar] variable indicating wether to apply (1) or not float(0) a log transformation of the parameters.
pars["logbase"]=np.array([10,10]) # [positive scalar] base given for the log transformation in the search space (costant for all transformed parameters)
pars["inset"]=np.array([0.2,1]) # [array of length npar] initial parameter set
pars["popsize"]=50 # [positive int scalar] number of solutions (individuals) drawn to form the initial and following populations
pars["ngen"]=20 # [positive int scalar] number of generations before ending the optimization
pars["obj_th"]=0.0005 # [positive scalar] the algorithm also stops if for gen_th generations: old_best-new_best<obj_th
pars["gen_th"]=50 # [positive scalar]
pars["mut_rate"]=0.3 # [scalar in [0,1]] fraction (probability) of the parameter values to be randomly drawn at each generation
pars["lin_rank"]=0 # [0/1 scalar] linear (1) / fitness-based (0) ranking switch
pars["num_jobs"]=1 # [positive scalar] cores used parallel job computing, 1 for linear computation 
pars["display"]=1 # [positive scalar] display the evolution over the function (2D only)
pars["disp_pts"]=100 # [positive scalar] number of points where to evaluate the objective function in each dimension (2 dims) for background display map 
pars["gnoise"]=np.array([0,0]) # [0/1 int array of length npar] variable indicating wether to apply (1) or not float(0) a gaussian noise to the new generation
#pars["noisevar"]=np.array([1,1])  # [float array of length npar] vector containing the variance of the noise to apply to each (only applied if gnoise==1)
pars["noisevar"]=abs(pars["ub"]-pars["lb"])*0.001 # [float array of length npar] vector containing the variance of the noise to apply to each (only applied if gnoise==1)
   

#%% objective funtion 1 (Hölder table)
pars["lb"]=np.array([-15,-15])
pars["ub"]=np.array([15,15])
#pars["noisevar"]=[0,0]#abs(pars["ub"]-pars["lb"])*0.01
pars["noisevar"]=abs(pars["ub"]-pars["lb"])*0.001
def objf(parset,i): # parset is an array like inset
    parobj=-abs(np.sin(parset[0])*np.cos(parset[1])*np.exp(abs(1-(np.sqrt(np.power(parset[0],2)+np.power(parset[1],2)))/pl.pi)))
    # time.sleep(3)
    return parobj
# run optimization
best=ga.optimize(pars,objf) # best is the vector of optimal parameters 

#%% objective funtion 2 (Easom function)
pars["lb"]=np.array([-100,-100])
pars["ub"]=np.array([100,100])
#pars["noisevar"]=np.array([10,10])
pars["noisevar"]=abs(pars["ub"]-pars["lb"])*0.001
def objf(parset,i): # parset is an array like inset
    parobj=-np.cos(parset[0])*np.cos(parset[1])*np.exp(-(np.power(parset[0]-pl.pi,2)+np.power(parset[1]-pl.pi,2)))
    # time.sleep(3)
    return parobj
# run optimization
best=ga.optimize(pars,objf) # best is the vector of optimal parameters 

#%% obj f 3 (Beale's function)
pars["lb"]=np.array([-4.5,-4.5])
pars["ub"]=np.array([4.5,4.5])
#pars["noisevar"]=np.array([1,1])
pars["noisevar"]=abs(pars["ub"]-pars["lb"])*0.01
def objf(parset,i): # parset is an array like inset
    parobj=np.power(1.5-parset[0]+parset[0]*parset[1],2)+np.power(2.25-parset[0]+parset[0]*np.power(parset[1],2),2)+np.power(2.265-parset[0]+parset[0]*np.power(parset[1],3),2)
    # time.sleep(3)
    return parobj
# run optimization
best=ga.optimize(pars,objf) # best is the vector of optimal parameters 

#%% obj f 4 
pars["lb"]=np.array([-200,-200])
pars["ub"]=np.array([200,200])
pars["noisevar"]=abs(pars["ub"]-pars["lb"])*0.001
def objf(parset,i): # parset is an array like inset
    parobj=parset[0]/parset[1]+parset[1]/parset[0]
    # time.sleep(3)
    return parobj
# run optimization
best=ga.optimize(pars,objf) # best is the vector of optimal parameters 